package co.bwsc.talker.server;

import java.io.IOException;

import co.bwsc.talker.base.request.LoginRequest;
import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.handler.RequestManager;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * This is server.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class Server extends AbstractServer{
	RequestManager manager = new RequestManager();
	public Server(int port) {
		super(port);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		manager.handleRequest(msg, client);
		
	}
}
