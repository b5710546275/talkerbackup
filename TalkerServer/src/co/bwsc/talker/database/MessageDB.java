package co.bwsc.talker.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sun.xml.internal.ws.api.model.MEP;

import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.MessageType;

/**This is message database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class MessageDB implements Database {
	Connection c;
	DatabaseFacade databaseFacade;
	private static final String format = "timeStamp,messageSenderUserID,messageContent,messageType,messageMediaURLs";
	private static final String tableName = "messageName";
	private static final String feild = "messageID INTEGER PRIMARY KEY AUTOINCREMENT, timeStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, messageSenderUserID INTEGER NOT NULL, messageContent TEXT NOT NULL, messageType INTERGER NOT NULL,messageMediaURLs TEXT NOT NULL";
	
	/**This is contractor of this class.*/
	public MessageDB() {
		this.databaseFacade = new DatabaseFacade();
		this.c = databaseFacade.connectToDB();
	}
	
	/**To create the table.*/
	public void createTable(){
		databaseFacade.createTable(tableName,feild);

	}
	
	/**To insert message in to the table.*/
	public void insertMessage(Message message) {
		String s = "";
		System.out.println(message.getMessageTimeStamp().getTime());
		s += message.getMessageTimeStamp().getTime() + ",";
		s += message.getMessageSenderUserID() + ",";
		s += suround(message.getMessageContent());
		s += suround(message.getMessageType().toString());
		s += "'" + message.getMessageMediaURLs() + "'";
		databaseFacade.insert(tableName, format, s);
	}
	
	/**To put single quote over message.*/
	public String suround(String s) {
		return "'" + s + "',";
	}
	
	/**
	 * Get the lastest message ID.
	 * @return lastest message ID.
	 */
	public int getLastestMessageID(){
		return databaseFacade.getLastestTableRow(tableName);
	}
	
	/**
	 * To get the message.
	 * @param id is the id.
	 * @return Message.
	 */
	public Message getMessage(int id) {
		List<Map> list = databaseFacade.select(tableName, "*",
				"WHERE  messageID =" + id);
		List<Message> messages = new ArrayList<Message>();
		Map m = list.get(0);

		 return new Message(id, new Date(((Long)m.get("timeStamp"))),
					((Integer)m.get("messageSenderUserID")),
					((String)m.get("messageContent")), MessageType.valueOf(((String)m.get("messageType").toString())),
					((String)m.get("messageMediaURLs")));
	}
//	public List<Message> getMessage(int id) {
//		List<Map> list = databaseFacade.select(tableName, "*",
//				"WHERE  messageID =" + id);
//		List<Message> messages = new ArrayList<Message>();
//		for(Map m:list){
//			messages.add(new Message(id, new Date(((Integer)m.get("timeStamp"))),
//					((Integer)m.get("messageSenderUserID")),
//					((String)m.get("messageContent")), MessageType.valueOf(((String)m.get("messageType").toString())),
//					((String)m.get("messageMediaURLs"))));
//		}
//		return messages;
//	}

}
