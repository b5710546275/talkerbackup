package co.bwsc.talker.database;

import static co.bwsc.talker.base.UserState.NORMAL;
import static co.bwsc.talker.base.UserStatus.AVAILABLE;
import static co.bwsc.talker.database.SqlCommand.CREATE;
import static co.bwsc.talker.database.SqlCommand.DROP;
import static co.bwsc.talker.database.SqlCommand.FROM;
import static co.bwsc.talker.database.SqlCommand.INSERT_INTO;
import static co.bwsc.talker.database.SqlCommand.SELECT;
import static co.bwsc.talker.database.SqlCommand.SET;
import static co.bwsc.talker.database.SqlCommand.TABLE;
import static co.bwsc.talker.database.SqlCommand.UPDATE;
import static co.bwsc.talker.database.SqlCommand.VALUES;
import static co.bwsc.talker.database.SqlCommand.WHERE;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.MessageType;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.User;

import com.sun.jmx.snmp.Timestamp;

/**
 * This is DatabaseFacade to execute command from the database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247 
 */
public class DatabaseFacade {
	public static void main(String args[]) {
		User user = new User("adam", NORMAL, AVAILABLE, 1005500);
		Chat chat = new Chat(110000);
		UserChatDB userChatDB = new UserChatDB();
		userChatDB.createTable();
		userChatDB.insert(user, chat);
		user = new User(1000001, "jame", NORMAL, AVAILABLE, 100500);
		userChatDB.insert(user, chat);
		chat = new Chat(1111111111);
		userChatDB.insert(user, chat);
		
		System.out.println(Arrays.toString(userChatDB.getUsers(110000)));
		DatabaseFacade databaseFacade = new DatabaseFacade();
		System.out.println(databaseFacade.getLastestTableRow("user"));
		
		
		System.out.println((new Timestamp(System.currentTimeMillis()))
				.getDate());
//		DatabaseFacade databaseFacade = new DatabaseFacade();
		Connection c = databaseFacade.connectToDB();
		
		UserPasswordDB userPasswordDB = new UserPasswordDB();
		userPasswordDB.createTable();
		userPasswordDB.insert("apple", "elppa");
		userPasswordDB.insert("apple", "elppa");
		System.out.println(userPasswordDB.checkPass("apple", "elppa"));
		System.out.println(userPasswordDB.changePass("apple", "1234"));

		UserDB userDB = new UserDB();
		userDB.createTable();
		
		
		databaseFacade.insert("user",
				"userID,userName, userProfileID, userState,userStatus",
				"100000001,'hello',100000001,'Normal','away'");
		databaseFacade.insert("user",
				"userName, userProfileID, userState,userStatus",
				"'apple',100000001,'Normal','away'");
		databaseFacade.update("user", "userName = 'a'", "userID = 1000001");
		System.out.println(databaseFacade.select("user", "*", ""));
		

		Profile profile = new Profile(10000, "wit", "wit543", "last",
				"forever");
		ProfileDB profileDB = new ProfileDB();
		profileDB.createTable();
		profileDB.insertProfile(profile);
		System.out.println(profileDB.getProfile(1));
		
		
		Message message = new Message(new Date(System.currentTimeMillis()),
				1000001, "hello", MessageType.PAIN, "");
		MessageDB messageDB = new MessageDB();
		messageDB.createTable();
		messageDB.insertMessage(message);
		System.out.println(messageDB.getMessage(1));
		user = new User("adam", NORMAL, AVAILABLE, 10055110);
		userDB.insertUser(user);
		System.out.println(userDB.getUser(100000001));
		System.out.println(userDB.getUser(100000001));
		user = new User(1000001, "jame", NORMAL, AVAILABLE, 10055100);
		userDB.updateUser(user);
		System.out.println(userDB.getUser(100000001));
		userDB.getUserName(100000001);
		
		System.out.println(userPasswordDB.update("apple", "1234"));
//
//		UserDB userDB = new UserDB();
//		userDB.createTable();
//		
//		
//		databaseFacade.insert("user",
//				"userID,userName, userProfileID, userState,userStatus",
//				"100000001,'hello',100000001,'Normal','away'");
//		databaseFacade.insert("user",
//				"userName, userProfileID, userState,userStatus",
//				"'apple',100000001,'Normal','away'");
//		databaseFacade.update("user", "userName = 'a'", "userID = 1000001");
//		System.out.println(databaseFacade.select("user", "*", ""));
//		
//
//		Profile profile = new Profile(10000l, "wit", "wit543", "last",
//				"forever");
//		ProfileDB profileDB = new ProfileDB();
//		profileDB.createTable();
//		profileDB.insertProfile(profile);
//		System.out.println(profileDB.getProfile(1));
//		
//		
//		Message message = new Message(new Date(System.currentTimeMillis()),
//				1000001l, "hello", MessageType.PAIN, "");
//		MessageDB messageDB = new MessageDB();
//		messageDB.createTable();
//		messageDB.insertMessage(message);
//		System.out.println(messageDB.getMessage(1l));
//		User user = new User("adam", NORMAL, AVAILABLE, 10055115500l);
//		userDB.insertUser(user);
//		System.out.println(userDB.getUser(100000001));
//		System.out.println(userDB.getUser(100000001));
//		user = new User(1000001, "jame", NORMAL, AVAILABLE, 10055115500l);
//		userDB.updateUser(user);
//		System.out.println(userDB.getUser(100000001));
//		userDB.getUserName(100000001);

	}
	
	/**To prepare before insert the value */
	public void prepare(Connection c) {
		String prepare = "";
		try {
			PreparedStatement preparedStatement = c.prepareStatement(prepare);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**To end the connection.*/
	public void closeConnection(Connection c) {
		try {
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	* To get lastest row from table.
	* @param tableName is that table.
	* @return the lastest row.
	*/
	public int getLastestTableRow(String tableName){
		Connection c = connectToDB();
		String command = "SELECT seq FROM SQLITE_SEQUENCE WHERE name = '"+tableName+"'";
		int length = -1;
		try {
			Statement stmt = c.createStatement();
			ResultSet rs =stmt.executeQuery(command);
			length = rs.getInt("seq");
			stmt.close();
			rs.close();
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return length;
	}
	
	/**To set the first row.*/
	public void setInitRowID(){
		Connection c = connectToDB();
		String command = "UPDATE SQLITE_SEQUENCE SET seq = 111111 WHERE name = 'user'";

		try {
			Statement stmt = c.createStatement();
			stmt.execute(command);
			stmt.close();
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	* To connect to the database.
	* @return connection.
	*/
	public Connection connectToDB() {
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:src\\db\\Talker.db");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Opened database successfully");
		return c;
	}

	/**
	* To create the table. 
	* @param tableName is name of the table.
	* @param header is name of title.
	*/
	public void createTable(String tableName, String header) {
		String command = CREATE + " " + TABLE + " " + tableName + " ("
				+ header.trim() + " );";
		boolean canCreate = true;
		Connection c = connectToDB();
		try {
			DatabaseMetaData metat = c.getMetaData();
			ResultSet res = metat.getTables(null, null, null,
					new String[] { "TABLE" });
			while (res.next()) {
				if (res.getString("TABLE_NAME").equals(tableName)) {
					canCreate = false;
				}
			}
			res.close();
			c.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		c = connectToDB();
		if (canCreate) {
			try {
				Statement stmt = c.createStatement();
				stmt.executeUpdate(command);
				stmt.close();

				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			String drop = DROP + " " + TABLE + " " + tableName + ";";
			try {
				Statement stmt = c.createStatement();
				stmt.executeUpdate(drop);
				stmt.executeUpdate(command);
				stmt.close();

				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	/**To not add the duplicate thing if table have it.*/
	public void insertIfNotExists(String tableName, String format, String value){
		List rs = select(tableName, "userName", "WHERE userName = "+value.split(",")[0]);
		if(rs.size()== 0){
			insert(tableName, format, value);
		}
	}
	
	/**To add thing in to table.*/
	public void insert(String tableName, String format, String value) {
		String command = INSERT_INTO + " " + tableName + " ( " + format.trim()
				+ ") " + VALUES + " (" + value.trim() + ");";
		Connection c = connectToDB();
		try {
			Statement stmt = c.createStatement();
			System.out.println(command);
			stmt.executeUpdate(command);
			stmt.close();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Records created successfully");
	}

	/**To add thing in to table.*/
	public void insert(String tableName, String format, String value,String condition) {
		String command = INSERT_INTO + " " + tableName + " ( " + format.trim()
				+ ") " + VALUES + " (" + value.trim()+") WHERE "+condition + ";";
		Connection c = connectToDB();
		try {
			Statement stmt = c.createStatement();
			stmt.executeUpdate(command);
			stmt.close();
			c.commit();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		System.out.println("Records created successfully");
	}
	
	/**
	* To get the data.
	* @param tableName is name of the table.
	* @param field is thing that want to select.
	* @param condition is is condition.
	* @return the data that selected.
	*/
	public List select(String tableName, String field, String condition) {
		String command = SELECT + " " + field + " " + FROM + " " + tableName
				+ " " + condition + ";";
		ResultSet rs = null;
		List list = new ArrayList<Map>();
		try {
			Connection c = connectToDB();
			Statement stmt = c.createStatement();
			System.out.println(command);
			rs = stmt.executeQuery(command);
			ResultSetMetaData md = rs.getMetaData();
			int columns = md.getColumnCount();
			
			while (rs.next()){
			     HashMap row = new HashMap(columns);
			     for(int i=1; i<=columns; ++i){           
			      row.put(md.getColumnName(i),rs.getObject(i));
			      System.out.println(md.getColumnName(i)+" "+rs.getObject(i)+" "+rs.getObject(i).getClass());
			     }
			      list.add(row);
			  }
			rs.close();
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Operation done successfully");
		return list;
		
	}
	/**
	* To update the data.
	* @param tableName is the name of table.
	* @param setTo is thing that want to update.
	* @param condition is the condition.
	* @return -1, that is to not change.
	*/
	public int update(String tableName, String setTo, String condition) {
		Connection c = connectToDB();
		System.out.println(condition);
		String command = UPDATE + " " + tableName + " " + SET + " " + setTo
				+ " " + WHERE + " " + condition + ";";
		// String command =
		// "UPDATE user set userName = 'aaaaaa' WHERE userID = 1000001";
		Statement stmt = null;
		try {
			System.out.println(command);
			stmt = c.createStatement();
			return stmt.executeUpdate(command);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
			stmt.close();
			c.close();
			}
			catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		}
		return -1;
	}
}
