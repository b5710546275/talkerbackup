package co.bwsc.talker.database;

import java.util.List;
import java.util.Map;

/**This is user friends database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class UserFriendsDB {
	private String format = "userID,friendID";
	private String header = "userID INTERGER NOT NULL, friendID INTEGER NOT NULL";
	private String tableName = "user_friend";
	private DatabaseFacade databaseFacade = new DatabaseFacade();
	
	/**To create table to dataFacade.*/
	public void createTable(){
		databaseFacade.createTable(tableName, header);
	}
	
	/**
	 * To insert the data to databaseFacade. 
	 * @param userID is user ID.
	 * @param friendID id friend ID. 
	 */
	public void insert(int userID, int friendID){
		databaseFacade.insert(tableName, format, userID+","+friendID);
	}
	
	/**
	 * To get data of friends. 
	 * @param userID is the ID to select.
	 * @return data of friends. 
	 */
	public int[] getFriend(int userID){
		List<Map> list = databaseFacade.select(tableName, "friendID", "WHERE userID = "+userID);
		int[] friends = new int[list.size()];
		for(int i =0;i<list.size();i++){
			friends[i] = (int) list.get(i).get("friendID");
		}
		return friends;
	}
}
