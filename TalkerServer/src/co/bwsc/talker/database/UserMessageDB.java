package co.bwsc.talker.database;

import java.util.List;
import java.util.Map;

/**This is user message database.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class UserMessageDB {
	private String formmat = "userID, messageID";
	private String tableName = "user_message";
	private DatabaseFacade databaseFacade = new DatabaseFacade();
	
	/**To create the table.*/
	public void createTable(){
		String header = "userID INTEGER NOT NULL, messageID INTEGER NOT NULL";
		databaseFacade.createTable(tableName, header);
	}
	
	/**
	 * To insert data into databaseFacade.
	 * @param userID is the user ID.
	 * @param messageID is message ID.
	 */
	public void insert(int userID, int messageID){
		databaseFacade.insert(tableName, formmat, userID+","+messageID);
	}
	
	/**
	 * To select the messageID.
	 * @param userID is user ID.
	 * @return the message ID.
	 */
	public int[] selectMessageIDs(int userID){
		List<Map> list = databaseFacade.select(tableName, "messageID", "WHERE userID ="+userID);
		return getIDArray(list, "messageID");
	}
	
	/**
	 * To select the user ID.
	 * @param messageID is message ID.
	 * @return the user ID.
	 */
	public int[] selecUserIDs(int messageID){
		List<Map> list = databaseFacade.select(tableName, "userID", "WHERE messageID = "+getClass());
		return getIDArray(list, "userID");
	}
	
	/**
	 * To get the array of ID.
     * @param list that want to get.
	 * @param field that want to get.
	 * @return array of ID.
	 */
	public int[] getIDArray(List<Map> list,String field){
		int[] ids = new int[list.size()];
		for(int i =0;i<list.size();i++){
			ids[i] = (int) list.get(i).get(field);
		}
		return ids;
	}
}
