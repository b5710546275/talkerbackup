package co.bwsc.talker.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.User;
import co.bwsc.talker.base.UserState;
import co.bwsc.talker.base.UserStatus;

/**This is user database. 
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class UserDB implements Database {
	private String format = "userName, userProfileID, userState,userStatus";
	private String feild = "userID INTEGER PRIMARY KEY AUTOINCREMENT, userName TEXT NOT NULL,userProfileID INT NOT NULL , userState TEXT NOT NULL, userStatus TEXT NOT NULL ";
	private String tableName = "user";
	DatabaseFacade databaseFacade;
	Connection c;
	
	/**This is contractor of this class. */
	public UserDB() {
		this.databaseFacade = new DatabaseFacade();	
	}	
	
	/**To create the table.*/
	public void createTable(){
		databaseFacade.createTable(tableName,feild);
	}
	
	/**To insert in to databaseFacade.*/
	public void insertUser(User user){
		String value = "";
		value+=suround(user.getUserName())+",";
		value+=user.getUserProfileID()+",";
		value+=suround(user.getUserState().toString())+",";
		value+=suround(user.getUserStatus().toString());
		databaseFacade.insert(tableName, format, value); 
	}
	/**
	 * To put single quote over message.
	 * @param s is the String.
	 * @return String the have quote over message.
	 */
	public String suround(String s){
		return "'"+s+"'";
	}
	
	/**
	 * To create user. 
	 * @param id is the ID from database.
	 * @return user.
 	 */
	public User getUser(int id){
		List<Map> list = databaseFacade.select(tableName, "*", "WHERE userId ="+id);
		User user = null;
		if(list.size()==0){
			throw new IllegalArgumentException();
		}
		Map m = list.get(0);
			user = new User(id,(String)m.get("userName"), UserState.getState((String)m.get("userState")), UserStatus.getStatus((String)m.get("userStatus")),( Integer)m.get("userProfileID"));
		return user;
	}
	
	/**
	 * Get the user.
	 * @param userName is user name.
	 * @return user.
	 */
	public User getUser(String userName){
		List<Map> list = databaseFacade.select(tableName, "*", "WHERE userName ="+suround(userName));
		User user = null;
		if(list.size()==0){
			throw new IllegalArgumentException();
		}
		Map m = list.get(0);
			user = new User((Integer)m.get("userID"),userName, UserState.getState((String)m.get("userState")), UserStatus.getStatus((String)m.get("userStatus")),( Integer)m.get("userProfileID"));
		return user;
	}
	
	/**
	 * Get the user name.
	 * @param id is user ID.
	 * @return user name.
	 */
	public String getUserName(int id){
		 List<Map> list = databaseFacade.select(tableName, "userName", "WHERE userID = "+id);
		 if(list.size()==0)
			 throw new IllegalArgumentException();
		 Map m = list.get(0);
			 String s =(String) m.get("userName");
			 System.out.println(s);
			return s;
	}
	
	/**
	 * To update the user.
	 * @param user is user that want to update.
	 */
	public void updateUser(User user){
		int userId = user.getUserID();
		String userName = user.getUserName();
		int userProfileID = user.getUserProfileID();
		UserState userState = user.getUserState();
		UserStatus userStatus = user.getUserStatus();
		String s = "userName ='"+userName+"',userProfileID = "+userProfileID+", userState ='"+userState+"',userStatus = '"+userStatus+"'";
		databaseFacade.update(tableName, s, "userID ="+userId);
		
	}
}
