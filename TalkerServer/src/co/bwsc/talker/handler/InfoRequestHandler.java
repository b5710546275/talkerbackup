package co.bwsc.talker.handler;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.User;
import co.bwsc.talker.base.request.InfoRequest;
import co.bwsc.talker.base.request.InfoRequestType;
import co.bwsc.talker.database.ChatMessageDB;
import co.bwsc.talker.database.MessageDB;
import co.bwsc.talker.database.ProfileDB;
import co.bwsc.talker.database.UserChatDB;
import co.bwsc.talker.database.UserDB;
import co.bwsc.talker.database.UserFriendsDB;
import co.bwsc.talker.database.UserMessageDB;

import com.lloseng.ocsf.server.ConnectionToClient;
import com.sun.swing.internal.plaf.metal.resources.metal;

/**This is class to handle info request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class InfoRequestHandler extends RequestHandler{

	private enum handleRequest{
		
		USER {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
					int userID = getID(client, manager);
					User user = userDB.getUser(userID);
					System.out.println(user);
				manager.sentToClient(user, client);
			}
		},
		PROFILE {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
				int userID = getID(client, manager);
				Profile profile = profileDB.getProfile(userID);
				manager.sentToClient(profile, client);
				
			}
		},
		FRIENDS {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
				int userID = getID(client, manager);
				int[] friendsID = userFriendsDB.getFriend(userID);
				manager.sentToClient(friendsID, client);
			}
		},
		CHATS {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
				int userID = getID(client, manager);
				int[] chatsID = userChatDB.getChat(userID);
				Chat[] chats = new Chat[chatsID.length];
				for(int i = 0;i<chats.length;i++){
					chats[i] = new Chat(chatsID[i]);
					chats[i].setUserIDs(userChatDB.getChat(chatsID[i]));
					chats[i].setMessageIDs(chatMessageDB.getMessages(chatsID[i]));
				}
				manager.sentToClient(chats, client);
			}
		},
		MESSAGES_IN_CHAT {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
				int userID = getID(client, manager);
				String chatID = msg.getRequestMessage().split("::")[1];
				int[] messageID =chatMessageDB.getMessages(Integer.parseInt(chatID));
				Message[] messages = new Message[messageID.length];
				for(int i = 0;i<messages.length;i++){
					messages[i] = messageDB.getMessage(messageID[i]);
					int[] userIDs= userMessageDB.selecUserIDs(messageID[i]);
					List list = new ArrayList<Integer>();
					list.addAll(Arrays.asList(userIDs));
					messages[i].setMessageViewedUserIDs(list);
				}
				manager.sentToClient(messages, client);
			}
		},
		USERS_IN_CHAT {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
				String chatID = msg.getRequestMessage().split("::")[1];
				int[] userIDs = userChatDB.getUsers(Integer.parseInt(chatID));
				manager.sentToClient(userIDs, client);
				
			}
		}, MEDIA_IN_MESSAGE {
			@Override
			void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager) {
				String messageID =msg.getRequestMessage().split("::")[1];
				String media = messageDB.getMessage(Integer.parseInt(messageID)).getMessageMediaURLs();
				manager.sentToClient(media, client);
				
			}
		};
		private final static UserDB userDB = new UserDB();
		private final static ProfileDB profileDB = new ProfileDB();
		private final static UserFriendsDB userFriendsDB = new UserFriendsDB();
		private final static UserChatDB userChatDB = new UserChatDB();
		private final static ChatMessageDB chatMessageDB = new ChatMessageDB();
		private final static MessageDB messageDB = new MessageDB();
		private final static UserMessageDB userMessageDB = new UserMessageDB();
		abstract void handle(InfoRequest msg, ConnectionToClient client, RequestManager manager);
		
		private static int getID(ConnectionToClient client, RequestManager manager){
			return manager.getConnectionMap().get(client).getUserID();
		}
	}
	@Override
	public void handle(Object msg, ConnectionToClient client, RequestManager manager) {
		InfoRequest infoRequest = (InfoRequest)msg;
		InfoRequestType type = infoRequest.getInfoType();
		handleRequest.valueOf(type.toString()).handle(infoRequest, client, manager);;
	}

}
