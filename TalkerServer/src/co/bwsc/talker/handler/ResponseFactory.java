package co.bwsc.talker.handler;

import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.response.ChatResponse;
import co.bwsc.talker.base.response.ChatResponseStatus;

/**
 * This class is for collecting method.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ResponseFactory {
	public ChatResponse chatResponse(Chat chat,ChatResponseStatus chatResponseStatus){
		return new ChatResponse(chat, chatResponseStatus);
	}
}
