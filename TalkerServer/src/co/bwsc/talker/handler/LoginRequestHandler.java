	package co.bwsc.talker.handler;

import co.bwsc.talker.base.User;
import co.bwsc.talker.base.request.LoginRequest;
import co.bwsc.talker.base.response.LoginResponse;
import co.bwsc.talker.base.response.LoginResponseStatus;
import co.bwsc.talker.database.UserDB;
import co.bwsc.talker.database.UserPasswordDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle login request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class LoginRequestHandler extends RequestHandler{
	private static final UserPasswordDB userPasswordDB = new UserPasswordDB();
	private static final UserDB userDB = new UserDB();
	@Override
	public void handle(Object msg, ConnectionToClient client,RequestManager manager) {
		LoginRequest loginRequest = (LoginRequest)msg;
		String userName = loginRequest.getUsername();
		String password = loginRequest.getPassword();
		LoginResponseStatus check = userPasswordDB.checkPass(userName, password);
		LoginResponse loginResponse = new LoginResponse(check);
		if(check==LoginResponseStatus.AUTH_OKAY){
			manager.addUser(getUser(userName), client);
		}
		manager.sentToClient(loginResponse, client);
	}
	private User getUser(String userName){
		return userDB.getUser(userName);
	}


}
  