package co.bwsc.talker.handler;

import co.bwsc.talker.base.request.AddFriendRequest;
import co.bwsc.talker.database.UserFriendsDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle add friend request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class AddFriendRequestHandler extends RequestHandler{
	UserFriendsDB UserFriendsDB = new UserFriendsDB();
	@Override
	public void handle(Object msg, ConnectionToClient client, RequestManager manager) {
		AddFriendRequest addFriendRequest = (AddFriendRequest) msg;
		int friendID = addFriendRequest.getTargetUserId();
		int userID = manager.getConnectionMap().get(client).getUserID();
		UserFriendsDB.insert(userID, friendID);
		UserFriendsDB.insert(friendID, userID);
		
		manager.sentToClient("succes", client);
		manager.notifyOther("new Friend", friendID);
	}
	
}
