package co.bwsc.talker.handler;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.request.*;
import co.bwsc.talker.base.*;

import com.lloseng.ocsf.server.ConnectionToClient;

/**
 * This is request manager.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class RequestManager {
	private final LoginRequestHandler loginRequestHandler = new LoginRequestHandler();
	private final SendMessageRequestHandler messageRequestHandler = new SendMessageRequestHandler();
	private final InfoRequestHandler infoRequestHandler = new InfoRequestHandler();
	private final AddFriendRequestHandler addFriendRequestHandler = new AddFriendRequestHandler();
	private final SendMessageRequestHandler sendMessageRequestHandler = new SendMessageRequestHandler();
	private final ProfileUpdateRequestHandler profileUpdateRequestHandler = new ProfileUpdateRequestHandler();
	private final PasswordChangeRequestHandler passwordChangeRequestHandler = new PasswordChangeRequestHandler();
	private List<ConnectionToClient> clients = new ArrayList<ConnectionToClient>();
	private Map<ConnectionToClient,User > connectionMap = new HashMap<ConnectionToClient,User>();
	
	/**
	 * To handle the request.
	 * @param msg is message.
	 * @param client is client.
	 */
	public void handleRequest(Object msg, ConnectionToClient client){
		if(!isInConnectionToClients(client)){
			clients.add(client);
		}
		if(msg instanceof Request){
			if(isClass(msg,LoginRequest.class)){
				loginRequestHandler.handle(msg, client, this);
			}
			else if(isClass(msg, InfoRequest.class)){
				infoRequestHandler.handle(msg, client, this);
			}
			else if(isClass(msg, AddFriendRequest.class)){
				addFriendRequestHandler.handle(msg, client, this);
			}
			else if(isClass(msg, SendMessageRequest.class)){
				sendMessageRequestHandler.handle(msg, client, this);
			}
			else if(isClass(msg, ProfileUpdateRequest.class)){
				profileUpdateRequestHandler.handle(msg, client, this);
			}
			else if(isClass(msg, PasswordChangeRequest.class)){
				passwordChangeRequestHandler.handle(msg, client, this);
			}
		}
	}
	
	/**
	 * To check that client is connect or not.
	 * @param client is client.
	 * @return true if client is connect , false if not.
	 */
	public boolean isInConnectionToClients(ConnectionToClient client){
		for(ConnectionToClient c :clients){
			if(c.equals(client)){
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * To check the class.
	 * @param msg is massage.
	 * @param clazz is class name.
	 * @return true if this is same class.
	 */
	private boolean isClass(Object msg,Class clazz){
		return msg.getClass() == clazz;
	}
	
	/**
	 * To send message to client.
	 * @param msg is message that want to send.
	 * @param client is client.
	 */
	public void sentToClient(Object msg, ConnectionToClient client){
		try {
			client.sendToClient(msg); 	 	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * To add user.
	 * @param user is user.
	 * @param client is client.
	 */
	public void addUser(User user, ConnectionToClient client){
		connectionMap.put(client,user );
	}
	
	/**
	  * Get the connectionMap.
	 * @return connectionMap.
	 */
	public Map<ConnectionToClient, User> getConnectionMap() {
		return connectionMap;
	}
	
	/**
	 * To check userID connect to client.
	 * @param userID is user ID.
	 * @return the next user if connect, null if not.
	 */
	public ConnectionToClient getUsetConnection(int userID){
		Iterator<ConnectionToClient> i =connectionMap.keySet().iterator();
		while(i.hasNext()){
			ConnectionToClient c =i.next();
			User user = connectionMap.get(c);
			if(user.getUserID() == userID){
				return c;
			}
		}
		return null;
	}
	
	/**
	 * To notify other.
	 * @param msg is message.
	 * @param userID is user ID.
	 */
	public void notifyOther(Object msg, int userID){
		ConnectionToClient connectionToClient = getUsetConnection(userID);
		sentToClient(msg, connectionToClient);
	}
}
