package co.bwsc.talker.handler;

import co.bwsc.talker.base.request.PasswordChangeRequest;
import co.bwsc.talker.database.UserPasswordDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle password change request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class PasswordChangeRequestHandler extends RequestHandler{
	UserPasswordDB userPasswordDB = new UserPasswordDB();
	@Override
	public void handle(Object msg, ConnectionToClient client,
			RequestManager manager) {
		PasswordChangeRequest passwordChangeRequest = (PasswordChangeRequest)msg;
		String newPass = passwordChangeRequest.getRequestMessage();
		String userName = manager.getConnectionMap().get(client).getUserName();
		int status = userPasswordDB.changePass(userName, newPass);
		
	}

}
