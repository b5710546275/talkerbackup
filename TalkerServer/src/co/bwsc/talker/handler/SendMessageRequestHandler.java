package co.bwsc.talker.handler;

import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.request.SendMessageRequest;
import co.bwsc.talker.database.ChatMessageDB;
import co.bwsc.talker.database.MessageDB;
import co.bwsc.talker.database.UserChatDB;

import com.lloseng.ocsf.server.ConnectionToClient;

/**This class is for handle send message request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class SendMessageRequestHandler extends RequestHandler {
	MessageDB messageDB = new MessageDB();
	ChatMessageDB chatMessageDB = new ChatMessageDB();
	UserChatDB userChatDB = new UserChatDB();
	@Override
	public void handle(Object msg, ConnectionToClient client, RequestManager manager) {
		SendMessageRequest sendMessageRequest =(SendMessageRequest) msg;
		Message message = sendMessageRequest.getMessage();
		
		int chatID = sendMessageRequest.getTargetChatId();
		messageDB.insertMessage(sendMessageRequest.getMessage());
		int messageID = messageDB.getLastestMessageID();
		chatMessageDB.insert(chatID, messageID);
		int[] userID = userChatDB.getUsers(chatID);
		notifyReciver(userID, manager);
	}
	
	/**
	 * To notify receiver.
	 * @param ids is ID.
	 * @param manager is who is send data.
	 * @param messageOneResponse is message to response.
	 */
	public void notifyReciver(int[] ids,RequestManager manager){
		String noti = "Alert";
		for(int i : ids){
			if(manager.getUsetConnection(i)!=null){
				manager.sentToClient(noti,manager.getUsetConnection(i));
			}
		}
	}
}
