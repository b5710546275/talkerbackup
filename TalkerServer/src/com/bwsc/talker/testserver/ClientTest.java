package com.bwsc.talker.testserver;

import java.io.IOException;

import co.bwsc.talker.base.request.LoginRequest;
import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.base.request.RequestType;

import com.lloseng.ocsf.client.AbstractClient;

/**
 * This is test of clients.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ClientTest extends AbstractClient{

	public ClientTest(String host, int port) {
		super(host, port);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		ClientTest clientTest = new ClientTest( "127.0.0.1", 5000);
		try {
			clientTest.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			clientTest.sendToServer("hello");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			clientTest.sendToServer(new LoginRequest("tor", "noop"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
