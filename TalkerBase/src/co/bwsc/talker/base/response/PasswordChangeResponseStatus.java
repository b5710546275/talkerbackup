package co.bwsc.talker.base.response;

/**
 * This is password change response status.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum PasswordChangeResponseStatus implements ResponseStatus {
	CHANGE_SUCCESSFUL, ILLEGAL_CHARACTER, SIMILAR_TO_OLD, CHANGE_FAILED;
}
