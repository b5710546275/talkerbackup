package co.bwsc.talker.base.response;

import java.io.Serializable;

/**
 * This is for response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 * @param <T> is general data type.
 */
public interface Response<T> extends Serializable {
	String getResponseMessage();
	T getResponseStatus();
	Object getResponseObject();
	ResponseType getType();
}
