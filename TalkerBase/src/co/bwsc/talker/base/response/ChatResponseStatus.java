package co.bwsc.talker.base.response;

/**
 * This is status of chat response. 
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum ChatResponseStatus implements ResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
