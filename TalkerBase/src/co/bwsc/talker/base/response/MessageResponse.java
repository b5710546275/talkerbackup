package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Message;

/**
 * This is message response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class MessageResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.MESSAGE;
	private MessageResponseStatus responseStatus;
	private Message responseObject;

	/**
	 * This is contractor.
	 * @param responseObject is message.
	 * @param responseStatus is status.
	 */
	public MessageResponse(Message responseObject, MessageResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}
 
	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public MessageResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public Message getResponseObject() {
		return responseObject;
	}

}
