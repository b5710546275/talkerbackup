package co.bwsc.talker.base.response;

/**
 * This is password change response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class PasswordChangeResponse implements Response {
	private static final long serialVersionUID = 8873614465748785319L;
	private final ResponseType type = ResponseType.PASSWORD_CHANGE;
	private PasswordChangeResponseStatus passwordChangeStatus;
	
	/**
	 * This is contractor.
	 * @param passwordChangeStatus is status.
	 */
	public PasswordChangeResponse(PasswordChangeResponseStatus passwordChangeStatus) {
		this.passwordChangeStatus = passwordChangeStatus;
	}
	
	@Override
	public String getResponseMessage() {
		return passwordChangeStatus.toString();
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public PasswordChangeResponseStatus getResponseStatus() {
		return passwordChangeStatus;
	}

	@Override
	public Object getResponseObject() {
		return null;
	}

}
