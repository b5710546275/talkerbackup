package co.bwsc.talker.base.response;

/**
 * This is type of response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum ResponseType {
	LOGIN, PASSWORD_CHANGE, USER, PROFILE, CHAT, MESSAGE, MEDIA;
}
