package co.bwsc.talker.base.response;

/**
 * This is login response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class LoginResponse implements Response {

	private static final long serialVersionUID = 5528966910763620354L;

	private final ResponseType type = ResponseType.LOGIN;
	
	private LoginResponseStatus responseStatus;

	/**
	 * This is contractor.
	 * @param responseStatus is status.
	 */
	public LoginResponse(LoginResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	
	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public LoginResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public Object getResponseObject() {
		return null;
	}
}
