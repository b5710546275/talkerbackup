package co.bwsc.talker.base.response;

/**
 * This is status of message response. 
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum MessageResponseStatus implements ResponseStatus {
	NORMAL, UNAUTHORIZED, FAIL;
}
