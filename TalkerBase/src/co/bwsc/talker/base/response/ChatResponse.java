package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Chat;
/**
 * This is chat response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ChatResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.CHAT;
	private ChatResponseStatus responseStatus;
	private Chat responseObject;

	/**
	 * This is contractor.
	 * @param responseObject is chat that response.
	 * @param responseStatus is status.
	 */
	public ChatResponse(Chat responseObject, ChatResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}

	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public ChatResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public Chat getResponseObject() {
		return responseObject;
	}

}
