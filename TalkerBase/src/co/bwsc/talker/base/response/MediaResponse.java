package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Message;

/**
 * This is media response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class MediaResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.MEDIA;
	private MediaResponseStatus responseStatus;
	private String responseObject;

	/**
	 * This is contractor.
	 * @param responseObject is String of Object.
	 * @param responseStatus is status.
	 */
	public MediaResponse(String responseObject, MediaResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}

	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public MediaResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public String getResponseObject() {
		return responseObject;
	}

}
