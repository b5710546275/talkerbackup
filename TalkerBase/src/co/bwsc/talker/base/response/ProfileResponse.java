package co.bwsc.talker.base.response;

import co.bwsc.talker.base.Profile;

/**
 * This is profile response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ProfileResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.PROFILE;
	private ProfileResponseStatus responseStatus;
	private Profile responseObject;

	/**
	 * This is contractor.
	 * @param responseObject is profile.
	 * @param responseStatus is status.
	 */
	public ProfileResponse(Profile responseObject, ProfileResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}

	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public ProfileResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public Object getResponseObject() {
		return responseObject;
	}

}
