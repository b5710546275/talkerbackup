package co.bwsc.talker.base.response;

import co.bwsc.talker.base.User;

/**
 * This is user response.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class UserResponse implements Response {

	private static final long serialVersionUID = 7070304147478274372L;
	private final ResponseType type = ResponseType.USER;
	private UserResponseStatus responseStatus;
	private User responseObject;

	/**
	 * This is contractor.
	 * @param responseObject is user.
	 * @param responseStatus is status.
	 */
	public UserResponse(User responseObject, UserResponseStatus responseStatus) {
		this.responseObject = responseObject;
		this.responseStatus = responseStatus;
	}

	@Override
	public String getResponseMessage() {
		return responseStatus.toString();
	}

	@Override
	public UserResponseStatus getResponseStatus() {
		return responseStatus;
	}

	@Override
	public ResponseType getType() {
		return type;
	}

	@Override
	public Object getResponseObject() {
		return responseObject;
	}

}
