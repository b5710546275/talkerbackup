package co.bwsc.talker.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**This is message.
* @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
*/
public class Message {
	private int messageID;
	private Date messageTimeStamp;
	private int messageSenderUserID;
	private List<Integer> messageViewedUserIDs;
	private String messageContent;
	private MessageType messageType;
	private String messageMediaURLs;
	
	/**
	 * This is contractor of this class.
	 * @param messageTimeStamp is message time. 
	 * @param messageSenderUserID is 
	 * @param messageContent
	 * @param messageType
	 * @param messageMediaURLs
	 */
	public Message(Date messageTimeStamp, int messageSenderUserID,
			String messageContent, MessageType messageType,
			String messageMediaURLs) {
		super();
		this.messageTimeStamp = messageTimeStamp;
		this.messageSenderUserID = messageSenderUserID;
		this.messageContent = messageContent;
		this.messageType = messageType;
		this.messageMediaURLs = messageMediaURLs;
		this.messageViewedUserIDs = new ArrayList<Integer>();
	}
	public Message(int messageID, Date messageTimeStamp,
			int messageSenderUserID, List<Integer> messageViewedUserIDs,
			String messageContent, MessageType messageType,
			String messageMediaURLs) {
		super();
		this.messageID = messageID;
		this.messageTimeStamp = messageTimeStamp;
		this.messageSenderUserID = messageSenderUserID;
		this.messageViewedUserIDs = messageViewedUserIDs;
		this.messageContent = messageContent;
		this.messageType = messageType;
		this.messageMediaURLs = messageMediaURLs;
	}
	public Message(int messageID, Date messageTimeStamp,
			int messageSenderUserID, String messageContent,
			MessageType messageType, String messageMediaURLs) {
		super();
		this.messageID = messageID;
		this.messageTimeStamp = messageTimeStamp;
		this.messageSenderUserID = messageSenderUserID;
		this.messageContent = messageContent;
		this.messageType = messageType;
		this.messageMediaURLs = messageMediaURLs;
		this.messageViewedUserIDs = new ArrayList<Integer>();
	}
	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	public Date getMessageTimeStamp() {
		return messageTimeStamp;
	}
	public void setMessageTimeStamp(Date messageTimeStamp) {
		this.messageTimeStamp = messageTimeStamp;
	}
	public int getMessageSenderUserID() {
		return messageSenderUserID;
	}
	public void setMessageSenderUserID(int messageSenderUserID) {
		this.messageSenderUserID = messageSenderUserID;
	}
	public List<Integer> getMessageViewedUserIDs() {
		return messageViewedUserIDs;
	}
	public void setMessageViewedUserIDs(List<Integer> messageViewedUserIDs) {
		this.messageViewedUserIDs = messageViewedUserIDs;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	public MessageType getMessageType() {
		return messageType;
	}
	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}
	public String getMessageMediaURLs() {
		return messageMediaURLs;
	}
	public void setMessageMediaURLs(String messageMediaURLs) {
		this.messageMediaURLs = messageMediaURLs;
	}
	@Override
	public String toString() {
		return "Message [messageID=" + messageID + ", messageTimeStamp="
				+ messageTimeStamp + ", messageSenderUserID="
				+ messageSenderUserID + ", messageViewedUserIDs="
				+ messageViewedUserIDs + ", messageContent=" + messageContent
				+ ", messageType=" + messageType + ", messageMediaURLs="
				+ messageMediaURLs + "]";
	}
	
	
}
