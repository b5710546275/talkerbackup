package co.bwsc.talker.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**This is chat.
* @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
*/
public class Chat {
	private int chatID;
	private List<Integer> userIDs = new ArrayList<Integer>();
	private List<Message> messages = new ArrayList<Message>();
	private List<Integer> messageIDs = new ArrayList<Integer>();

	public Chat() {
	}
	
	/**This is contractor.*/
	public Chat(int chatID) {
		this.chatID = chatID;
	}
	
	/**
	 * To get ID of user.
	 * @return ID of user.
	 */
	public List<Integer> getUserIDs() {
		return userIDs;
	}
	
	/**
	 * To set the user ID.
	 * @param userIDs is user ID that want to change to.
	 */
	public void setUserIDs(List<Integer> userIDs) {
		this.userIDs = userIDs;
	}
	
	/**
	 * To set all the user ID.
	 * @param usetIDs is array of user ID that want to change to.
	 */
	public void setUserIDs(int[] usetIDs) {
		this.userIDs.removeAll(userIDs);
		this.userIDs.addAll(userIDs);
	}

	/**
	 * To get the message ID from List.
	 * @return message ID.
	 */
	public List<Integer> getMessageIDs() {
		return messageIDs;
	}
	
	/**
	 * To get the message from List.
	 * @return message.
	 */
	public List<Message> getMessages() {
		return messages;
	}
	
	/**
	 * To set the message ID from List.
	 * @param messageIDs is ID that want to change to.
	 */
	public void setMessageIDs(List<Integer> messageIDs) {
		this.messageIDs = messageIDs;
	}

	/**
	 * To set the message ID from array.
	 * @param messagesIDs is ID that want to change to.
	 */
	public void setMessageIDs(int[] messagesIDs) {
		this.messageIDs.clear();
		this.messageIDs.addAll(messageIDs);
	}
	
	/**
	 * To set message. 
	 * @param messages is message from the List.
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	/**
	 * To set message.
	 * @param messages is message from array.
	 */
	public void setMessages(Message[] messages) {
		this.messages.clear();
		this.messages.addAll(Arrays.asList(messages));
	}
	
	/**
	 * To add user.
	 * @param userID is user ID.
	 */
	public void addUser(Integer userID) {
		this.userIDs.add(userID);
	}

	/**
	 * To remove the user.
	 * @param user is user that want to remove.
	 */
	public void removeUser(User user) {
		this.userIDs.remove(user);
	}
	
	/**
	 * To remove the user.
	 * @param userID is user that want to remove.
	 */
	public void removeUser(int userID) {
		for (int u : userIDs) {
			if (u == userID) {
				removeUser(u);
			}
		}
	}
	
	/**
	 * To add the message.
	 * @param message is message that want to add.
	 */
	public void addMessageID(Integer message) {
		this.messageIDs.add(message);
	}

	/**
	 * To get the chat ID.
	 * @return chatID.
	 */
	public int getChatID() {
		return chatID;
	}
	
	/**
	 * To set the chat ID.
	 * @param chatID is ID of chat.
	 */
	public void setChatID(int chatID) {
		this.chatID = chatID;
	}

}
