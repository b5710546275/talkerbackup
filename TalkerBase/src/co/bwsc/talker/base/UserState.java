package co.bwsc.talker.base;

/**
 * This is user state class.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum UserState {
	DEACTIVATED("Deactivated"),
	NORMAL("Normal"),
	BANNED("Banned");
	public final String state;
	
	private UserState(String state) {
		this.state = state;
	}
	
	/**
	 * To return the state.
	 */
	public String toString(){
		return state;
	}
	public static UserState getState(String name){
		for(UserState s :UserState.values()){
			if(s.toString().equalsIgnoreCase(name)){
				return s;
			}
			
		}
		throw new IllegalArgumentException();
	}
}
