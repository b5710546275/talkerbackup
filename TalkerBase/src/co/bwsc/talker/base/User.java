package co.bwsc.talker.base;

import java.util.ArrayList;
import java.util.List;

/**
 * This is user class.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class User {
	private int userID;
	private String userName;
	private UserState userState;
	private UserStatus userStatus;
	private int userProfileID;
	private List<Integer> friends = new ArrayList<Integer>();
	
	/**
	 * This is contractor of this class.
	 * @param userID is user ID.
	 * @param userName is user name.
	 * @param userState is user state.
	 * @param userStatus is user status.
	 * @param userProfileID is user profile ID.
	 */
	public User(int userID, String userName, UserState userState, UserStatus userStatus,int userProfileID) {
		this.userID = userID;
		this.userName = userName;
		this.userState = userState;
		this.userStatus = userStatus;
		this.userProfileID = userProfileID;
	}
	
	/**
	 * This is contractor of this class.
	 * @param userName is user name. 
	 * @param userState is user state.
	 * @param userStatus is user status.
	 * @param userProfileID is user profile ID.
	 */
	public User(String userName, UserState userState, UserStatus userStatus,
			int userProfileID) {
		super();
		this.userName = userName;
		this.userState = userState;
		this.userStatus = userStatus;
		this.userProfileID = userProfileID;
	}
	
	/**
	 * To get list of friends.
	 * @return list of friends.
	 */
	public List getFriends() {
		return friends;
	}

	/**
	 * Set the list of friend.
	 * @param friends is list that want to set.
	 */
	public void setFriends(List friends) {
		this.friends = friends;
	}
	
	/**
	 * Get the user name.
	 * @return String of user name.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Set the user name.
	 * @param userName is user name that want to set to.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Get the user state.
	 * @return user state.
	 */
	public UserState getUserState() {
		return userState;
	}
	
	/**
	 * Set the user state.
	 * @param userState is the state.
	 */
	public void setUserState(UserState userState) {
		this.userState = userState;
	}
	
	/**
	 * Get the user status.
	 * @return user status.
	 */
	public UserStatus getUserStatus() {
		return userStatus;
	}

	/**
	 * Set the user status.
	 * @param userStatus is the status.
	 */
	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	/**
	 * Get the user profile ID.
	 * @return user profile ID.
	 */
	public int getUserProfileID() {
		return userProfileID;
	}
	
	/**
	 * Set the user profile ID.
	 * @param userProfileID is user profile ID.
	 */
	public void setUserProfileID(int userProfileID) {
		this.userProfileID = userProfileID;
	}

	/**
	 * Get user ID.
	 * @return user ID.
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * Set user ID.
	 * @param userID is user ID.
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", userState=" + userState
				+ ", userStatus=" + userStatus + ", userProfileID="
				+ userProfileID + "]";
	}
	
	
}
