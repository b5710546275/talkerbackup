package co.bwsc.talker.base;
/**
 * This is message type.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 *
 */
public enum MessageType {
	PAIN(0),
	WITHMEDIA(1),
	NUGE(2);
	public final int type;
	private MessageType(int type) {
		this.type = type;
	}
}
