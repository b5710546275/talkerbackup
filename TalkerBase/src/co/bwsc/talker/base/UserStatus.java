package co.bwsc.talker.base;

/**
 * This is user status class.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 *
 */
public enum UserStatus {
	OFFLINE,
	AVAILABLE,
	AWAY,
	BUSY;	
	public static UserStatus getStatus(String name){
		for(UserStatus s: UserStatus.values()){
			if(s.toString().equalsIgnoreCase(name)){
				return s;
			}
		}
		throw new IllegalArgumentException();
	}
}
