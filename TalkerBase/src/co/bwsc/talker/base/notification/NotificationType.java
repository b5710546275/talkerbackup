package co.bwsc.talker.base.notification;

/**
 * This is notification type.
 *  @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 *
 */
public enum NotificationType {
	NEW_MESSAGE;

}
