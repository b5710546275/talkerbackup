package co.bwsc.talker.base.notification;

/**
 * This is interface for notification. 
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 *
 */
public interface Notification {
	String getNotificationMessage();
	Object getNotificationObject();
	NotificationType getType();
}
