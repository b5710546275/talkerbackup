package co.bwsc.talker.base.request;

/**
 * This is type of request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum RequestType {
	LOGIN, INFO, ADD_FRIEND, SEND_MESSAGE, PROFILE_UPDATE, PASSWORD_CHANGE;
}