package co.bwsc.talker.base.request;

import co.bwsc.talker.base.Profile;

/**
 * This is profile update request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ProfileUpdateRequest implements Request {

	private static final long serialVersionUID = -3052932409965739758L;

	private final RequestType type = RequestType.PROFILE_UPDATE;
	
//	private String displayName, firstName, lastName, bio, photoURL;
	private Profile profile;
//	public ProfileUpdateRequest(String displayName, String firstName,
//			String lastName, String bio, String photoURL) {
//		this.displayName = displayName;
//		this.firstName = firstName;
//		this.lastName = lastName;
//		this.bio = bio;
//		this.photoURL = photoURL;
//	}
	
	/**
	 * This is contractor.
	 * @param profile is profile.
	 */
	public ProfileUpdateRequest(Profile profile) {
		this.profile = profile;
	}
	@Override
	public String getRequestMessage() {
//		return displayName + "::" + firstName + "::" + lastName + "::" + bio + "::" + photoURL;
		return profile.toString();
	}

	@Override
	public RequestType getType() {
		return type;
	}
	/**
	 * To get the profile.
	 * @return profile.
	 */
	public Profile getProfile(){
		return profile;
	}
}
