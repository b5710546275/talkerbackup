package co.bwsc.talker.base.request;

/**
 * This class is for add friend.
 *  @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class AddFriendRequest implements Request {

	private static final long serialVersionUID = -1101160405951977560L;

	private final RequestType type = RequestType.ADD_FRIEND;
	
	private int targetUserId;
	
	/**
	 * To find target friend.
	 * @param targetUserId is the user ID.
	 */
	public AddFriendRequest(int targetUserId) {
		this.targetUserId = targetUserId;
	}
	
	/**
	 * Get the target user ID.
	 * @return target user ID.
	 */
	public int getTargetUserId() {
		return targetUserId;
	}
	
	@Override
	public String getRequestMessage() {
		return Integer.toString(targetUserId);
	}

	@Override
	public RequestType getType() {
		return type;
	}

}
