package co.bwsc.talker.base.request;

/**
 * This is Login request. 
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class LoginRequest implements Request {
	
	private static final long serialVersionUID = 8247678460486309926L;
	private final RequestType type = RequestType.LOGIN;
	private String username, password;
	
	/**
	 * This is contractor of this class.
	 * @param username is username.
	 * @param password is password.
	 */
	public LoginRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}
	@Override
	public String getRequestMessage() {
		return username + "::" + password;
	}
	@Override
	public RequestType getType() {
		return type;
	}
	
	/**
	 * To get the user name.
	 * @return user name.
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * To get the password.
	 * @return password.
	 */
	public String getPassword() {
		return password;
	}
	
}
