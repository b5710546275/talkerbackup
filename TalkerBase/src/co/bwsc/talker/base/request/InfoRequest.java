package co.bwsc.talker.base.request;

/**
 * This is for information request.
 *  @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 *
 */
public class InfoRequest implements Request {

	private static final long serialVersionUID = -1083804300381602135L;

	private final RequestType type = RequestType.INFO;
	private InfoRequestType infoType;
	private String requestMessage;

	/**
	 * To request to be friend.
	 * @param infoType is type of information.
	 * @param requestMessage is message that send to target.
	 */
	public InfoRequest(InfoRequestType infoType, String requestMessage) {
		this.infoType = infoType;
		this.requestMessage = infoType.toString() + "::" + requestMessage;
	}

	/**
	 * This is contractor of this class.
	 * @param infoType is type of information. 
	 */
	public InfoRequest(InfoRequestType infoType) {
		this.infoType = infoType;
		this.requestMessage = infoType.toString();
	}

	/**
	 * Get the type of information.
	 * @return type of information.
	 */
	public InfoRequestType getInfoType() {
		return infoType;
	}

	@Override
	public String getRequestMessage() {
		return requestMessage;
	}

	@Override
	public RequestType getType() {
		return type;
	}
}
