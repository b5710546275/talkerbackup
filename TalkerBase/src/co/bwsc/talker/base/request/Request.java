package co.bwsc.talker.base.request;

import java.io.Serializable;

/**
 * This is interface for request.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public interface Request extends Serializable {
	String getRequestMessage();
	RequestType getType();
}