package co.bwsc.talker.base.request;

/**
 * This is for password change request.	
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class PasswordChangeRequest implements Request {
	
	private static final long serialVersionUID = 6295110362114483290L;

	private final RequestType type = RequestType.PASSWORD_CHANGE;
	
	private String newPassword;
	
	/**
	 * This is contractor of this class.
	 * @param newPassword is new password.
	 */
	public PasswordChangeRequest(String newPassword) {
		this.newPassword = newPassword;
	}
	
	@Override
	public String getRequestMessage() {
		return newPassword;
	}

	@Override
	public RequestType getType() {
		return type;
	}

}
