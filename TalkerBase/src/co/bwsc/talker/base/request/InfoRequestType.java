package co.bwsc.talker.base.request;

/**
 *  This is information request type. 
 *  @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public enum InfoRequestType {
	USER, PROFILE, FRIENDS, CHATS, MESSAGE, MESSAGES_IN_CHAT, USERS_IN_CHAT, MEDIA_IN_MESSAGE;
}
