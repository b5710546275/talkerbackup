package co.bwsc.talker.base.request;

import co.bwsc.talker.base.Message;

/**
 * This is send message request class.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 *
 */
public class SendMessageRequest implements Request {
	
	private static final long serialVersionUID = -8105733336822804378L;

	private final RequestType type = RequestType.SEND_MESSAGE;
	
	private Message message;
	
	private int targetChatId;
	
	/**
	 * This is contractor.
	 * @param targetChatId is user that want to send message.
	 * @param message is the message.
	 */
	public SendMessageRequest(int targetChatId, Message message) {
		this.targetChatId = targetChatId;
		this.message = message;
	}
	
	@Override
	public String getRequestMessage() {
		return targetChatId + "::" + message;
	}

	@Override
	public RequestType getType() {
		return type;
	}

	public Message getMessage() {
		return message;
	}

	/**
	 * Get the ID of target chat.
	 * @return ID of target chat.
	 */
	public int getTargetChatId() {
		return targetChatId;
	}
	
	

}
