package co.bwsc.talker.base;
/**
 * This is profile class.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class Profile {
	private int proFileID;
	private String profileDisplayName;
	private String profileFirstname;
	private String profileLastName;
	private String profileBio;
	private String profilePhotoURL;
	public Profile(int proFileID, String profileDisplayName,
			String profileFirstname,
			String profileLastName, String profileBio) {
		super();
		this.proFileID = proFileID;
		this.profileDisplayName = profileDisplayName;
		this.profileFirstname = profileFirstname;
		this.profileLastName = profileLastName;
		this.profileBio = profileBio;
	}
	
	/**
	 * This is contractor of this class.
	 * @param profileDisplayName is display name.
	 * @param profileFirstname is first name.
	 * @param profileLastName is last name.
	 * @param profileBio 
	 * @param profilePhotoURL
	 */
	public Profile(String profileDisplayName, String profileFirstname,
			String profileLastName, String profileBio, String profilePhotoURL) {
		super();
		this.profileDisplayName = profileDisplayName;
		this.profileFirstname = profileFirstname;
		this.profileLastName = profileLastName;
		this.profileBio = profileBio;
		this.profilePhotoURL = profilePhotoURL;
	}
	
	/**
	 * This is contractor of this class.
	 * @param proFileID is profile ID.
	 * @param profileDisplayName is display name for profile.
	 * @param profileFirstname is first name.
	 * @param profileLastName is last name.
	 * @param profileBio
	 * @param profilePhotoURL is profile picture.
	 */
	public Profile(int proFileID, String profileDisplayName,
			String profileFirstname, String profileLastName, String profileBio,
			String profilePhotoURL) {
		super();
		this.proFileID = proFileID;
		this.profileDisplayName = profileDisplayName;
		this.profileFirstname = profileFirstname;
		this.profileLastName = profileLastName;
		this.profileBio = profileBio;
		this.profilePhotoURL = profilePhotoURL;
	}
	
	/**
	 * Get directory of profile picture.
	 * @return String of picture directory.
	 */
	public String getProfilePhotoURL() {
		return profilePhotoURL;
	}
	
	/**
	 * Set directory of profile picture.
	 * @param profilePhotoURL is String of picture directory.
	 */
	public void setProfilePhotoURL(String profilePhotoURL) {
		this.profilePhotoURL = profilePhotoURL;
	}
	
	/**
	 * Get profile display name.
	 * @return String of profile display name.
	 */
	public String getProfileDisplayName() {
		return profileDisplayName;
	}
	
	/**
	 * Set profile display name.
	 * @param profileDisplayName is profile display name.
	 */
	public void setProfileDisplayName(String profileDisplayName) {
		this.profileDisplayName = profileDisplayName;
	}
	
	/**
	 * Get profile last name.
	 * @return profile last name.
	 */
	public String getProfileLastName() {
		return profileLastName;
	}
	
	/**
	 * Set profile last name.
	 * @param profileLastName is profile last name.
	 */
	public void setProfileLastName(String profileLastName) {
		this.profileLastName = profileLastName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getProfileBio() {
		return profileBio;
	}
	
	/**
	 * 
	 * @param profileBio
	 */
	public void setProfileBio(String profileBio) {
		this.profileBio = profileBio;
	}
	
	/**
	 * Get profile ID.
	 * @return profile ID.
	 */
	public int getProFileID() {
		return proFileID;
	}
	
	/**
	 * Get the profile first name.
	 * @return profile first name.
	 */
	public String getProfileFirstname() {
		return profileFirstname;
	}
	
	/**
	 * Set the profile first name.
	 * @param profileFirstname is profile first name.
	 */
	public void setProfileFirstname(String profileFirstname) {
		this.profileFirstname = profileFirstname;
	}
	
	/**
	 * Set the profile ID.
	 * @param proFileID is profile ID.
	 */
	public void setProFileID(int proFileID) {
		this.proFileID = proFileID;
	}
	@Override
	public String toString() {
		return "Profile [proFileID=" + proFileID + ", profileDisplayName="
				+ profileDisplayName + ", profileFirstname=" + profileFirstname
				+ ", profileLastName=" + profileLastName + ", profileBio="
				+ profileBio + ", profilePhotoURL=" + profilePhotoURL + "]";
	}
	
	
}
