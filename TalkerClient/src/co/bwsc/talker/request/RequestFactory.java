package co.bwsc.talker.request;


import sun.security.util.Password;
import co.bwsc.talker.base.Message;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.request.AddFriendRequest;
import co.bwsc.talker.base.request.InfoRequest;
import co.bwsc.talker.base.request.InfoRequestType;
import co.bwsc.talker.base.request.LoginRequest;
import co.bwsc.talker.base.request.PasswordChangeRequest;
import co.bwsc.talker.base.request.ProfileUpdateRequest;
import co.bwsc.talker.base.request.SendMessageRequest;

/**
 * This is server request factory.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class RequestFactory {
	
	/**
	 * To login.
	 * @param username is user name.
	 * @param password is password.
	 * @return loginRequest.
	 */
	public LoginRequest requestLogin(String username,String password){
		LoginRequest loginRequest = new LoginRequest(username, password);
		return loginRequest;
		
	}
	
	/**
	 * To add friend.
	 * @param friendID is ID of friend
	 * @return addFriendRequest. 
	 */
	public AddFriendRequest requestAddFriend(int friendID){
		AddFriendRequest addFriendRequest = new AddFriendRequest(friendID);
		return addFriendRequest;
	}
	
	/**
	 * To request information.
	 * @param infoType is type of the information.
	 * @return infoRequest.
	 */
	public InfoRequest requestInfo(InfoRequestType infoType){
		InfoRequest infoRequest = new InfoRequest(infoType);
		return infoRequest;
	}
	
	/**
	* To request information.
	 * @param infoType is type of the information.
	 * @param message is message
	 * @return infoRequest.
	 */
	public InfoRequest requestInfo(InfoRequestType infoType, String message){
		InfoRequest infoRequest = new InfoRequest(infoType,message);
		return infoRequest;
	}
	
	/**
	 * To update profile.
	 * @param profile is profile that want to update.
	 * @return profileUpdateRequest.
	 */
	public ProfileUpdateRequest requstUpdateProfile(Profile profile){
		ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest(profile);
		return profileUpdateRequest;
	}
	
	/**
	 * To change password. 
	 * @param newPassword is String of new password.
	 * @return passwordChangeRequest.
	 */ 
	public PasswordChangeRequest requestPasswordChange(String newPassword){
		PasswordChangeRequest passwordChangeRequest = new PasswordChangeRequest(newPassword);
		return passwordChangeRequest;
	}
	
	/**
	 * To send request message.
	 * @param targetChatId is ID of user that want to send.
	 * @param message is message.
	 * @return sendMessageRequest.
	 */
	public SendMessageRequest requestSendMessage(int targetChatId,Message message){
		SendMessageRequest sendMessageRequest = new SendMessageRequest(targetChatId, message);
		return sendMessageRequest;
	}
}
