package co.bwsc.talker.request;

import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.connector.Server;

/**
 * This is server request manager.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class RequestManager {
	
	public int sendRequest(Request request, Server server) {
		return server.sentToServer(request);
	}

}
