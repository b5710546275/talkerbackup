package co.bwsc.talker.connector;

import java.io.IOException;

import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.request.RequestManager;
import co.bwsc.talker.responseHandler.ResponseManager;

/**
 * This is server controller.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ServerController {
	private Server server;
	private RequestManager requestManager;
	
	/**
	 * This is contractor of this class.
	 * @param host is name of host.
	 * @param port is number of port.
	 * @param responseManager is ResponseManager.
	 */
	public ServerController(String host,int port , ResponseManager responseManager) {
		System.out.println(host+" "+port);
		server = new Server(host, port,responseManager);
		requestManager = new RequestManager();
	}
	
	/**
	 * To send the request.
	 * @param request is request.
	 * @return the request.
	 */
	public int sentRequest(Request request){
		return requestManager.sendRequest(request, server);
	}
	
	/**
	 * To get the server.
	 * @return server
	 */
	public Server getServer() {
		return server;
	}
	
	/**
	 * To get requestManager.
	 * @return requestManager.
	 */
	public RequestManager getRequestManager() {
		return requestManager;
	}
	
	/**To connect.*/
	public void connect(){
		try {
			this.server.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
