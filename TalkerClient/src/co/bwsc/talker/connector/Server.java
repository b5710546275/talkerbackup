package co.bwsc.talker.connector;

import java.io.IOException;

import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.responseHandler.ResponseManager;

import com.lloseng.ocsf.client.AbstractClient;

/**
 * This is server.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class Server extends AbstractClient {
	ResponseManager responseManager;
	
	/**
	 * This is contractor.
	 * @param host is host.
	 * @param port is port number. 
	 * @param responseManager is ResponseManager.
	 */
	public Server(String host, int port, ResponseManager responseManager) {
		super(host, port);
		this.responseManager = responseManager;
	}

	@Override
	protected void handleMessageFromServer(Object msg) {
		System.out.println(msg);
		responseManager.handleResponse(msg);
	}

	/**
	 * To send data to server.
	 * @param request is the request that want to send to.
	 * @return 0 if can send, if not return -1.
	 */
	public int sentToServer(Request request){
		try {
			sendToServer(request);
		} catch (IOException e) {
			return -1;
		}
		return 0;
	}
}
