package co.bwsc.talker.responseHandler;

import sun.launcher.resources.launcher;
import sun.rmi.runtime.Log;
import sun.util.logging.resources.logging;
import co.bwsc.talker.base.response.ChatResponseStatus;
import co.bwsc.talker.base.response.LoginResponseStatus;
import co.bwsc.talker.base.response.MediaResponseStatus;
import co.bwsc.talker.base.response.MessageResponseStatus;
import co.bwsc.talker.base.response.PasswordChangeResponseStatus;
import co.bwsc.talker.base.response.ProfileResponseStatus;
import co.bwsc.talker.base.response.UserResponseStatus;
import co.bwsc.talker.client.Client;

/**
 * This is server response manager.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class ResponseManager  {
	private static Client client;
	
	/**
	 * This is contractor. 
	 * @param client is Client.
	 */
	public ResponseManager(Client client) {
		this.client = client;
	}
	enum type {
		LoginResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.LoginResponse loginResponse = (co.bwsc.talker.base.response.LoginResponse) msg;
				LoginResponseStatus loginResponseStatus =loginResponse.getResponseStatus();
				if(loginResponseStatus == LoginResponseStatus.AUTH_OKAY){
					client.setLogined(true);
				}
				else{
					client.canNotLogin(loginResponseStatus);
				}
				System.out.println(loginResponseStatus);
			}
		},
		PasswordChangeResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.PasswordChangeResponse passwordChangeResponse = (co.bwsc.talker.base.response.PasswordChangeResponse)msg;
				PasswordChangeResponseStatus passwordChangeResponseStatus = passwordChangeResponse.getResponseStatus();
				if(passwordChangeResponseStatus == PasswordChangeResponseStatus.CHANGE_SUCCESSFUL){
					
				}
				else{
					
				}
			}
		},
		UserResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.UserResponse userResponse = (co.bwsc.talker.base.response.UserResponse)msg;
				UserResponseStatus userResponseStatus = userResponse.getResponseStatus();
				if(userResponseStatus == UserResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
		},
		ProfileResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.ProfileResponse profileResponse = (co.bwsc.talker.base.response.ProfileResponse)msg;
				ProfileResponseStatus profileResponseStatus = profileResponse.getResponseStatus();
				if(profileResponseStatus == ProfileResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
		},
		ChatResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.ChatResponse chatResponse = (co.bwsc.talker.base.response.ChatResponse)msg;
				ChatResponseStatus chatResponseStatus = chatResponse.getResponseStatus();
				if(chatResponseStatus == ChatResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
		},
		MediaResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.MediaResponse mediaResponse = (co.bwsc.talker.base.response.MediaResponse)msg;
				MediaResponseStatus mediaResponseStatus = mediaResponse.getResponseStatus();
				if(mediaResponseStatus == MediaResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
		},
		MessageResponse {
			@Override
			void handle(Object msg) {
				co.bwsc.talker.base.response.MessageResponse messageResponse = (co.bwsc.talker.base.response.MessageResponse)msg;
				MessageResponseStatus messageResponseStatus = messageResponse.getResponseStatus();
				if(messageResponseStatus == MessageResponseStatus.NORMAL){
					
				}
				else{
					
				}
			}
		}
		;
		abstract void handle(Object msg);
	}
	
	/**
	 * To handle response.
	 * @param msg is object.
	 */
	public void handleResponse(Object msg){
		type.valueOf(msg.getClass().getSimpleName().toString()).handle(msg);
	}
}
