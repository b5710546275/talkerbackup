	package co.bwsc.talker.responseHandler;

import java.util.Observable;

public abstract class ResponseHandler<T> extends Observable{
	public abstract void handle(T msg);
}
