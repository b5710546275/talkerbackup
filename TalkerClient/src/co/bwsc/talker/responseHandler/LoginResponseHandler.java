package co.bwsc.talker.responseHandler;

import co.bwsc.talker.base.response.LoginResponse;
import co.bwsc.talker.base.response.LoginResponseStatus;

public class LoginResponseHandler extends ResponseHandler<LoginResponse>{

	@Override
	public void handle(LoginResponse msg) {
		notifyObservers(this);
	}

}
