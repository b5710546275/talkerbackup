package co.bwsc.talker.client;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import sun.rmi.runtime.Log;
import co.bwsc.talker.base.Chat;
import co.bwsc.talker.base.Profile;
import co.bwsc.talker.base.User;
import co.bwsc.talker.base.request.InfoRequestType;
import co.bwsc.talker.base.request.Request;
import co.bwsc.talker.base.request.RequestType;
import co.bwsc.talker.base.response.LoginResponseStatus;
import co.bwsc.talker.connector.ServerController;
import co.bwsc.talker.request.RequestFactory;
import co.bwsc.talker.request.RequestManager;
import co.bwsc.talker.responseHandler.ResponseManager;

import com.sun.jmx.remote.internal.ServerCommunicatorAdmin;

/**
 * This is client.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class Client {
	private ServerController serverController;
	private RequestManager requestManager;
	private RequestFactory requestFactory;
	private ResponseManager responseManager = new ResponseManager(this);
	private User user ;
	private List<Chat> chats;
	private	List<Profile> friends;
	private boolean logined = false;
	Client(){
		serverController = new ServerController("localhost", 5555, responseManager);
		requestFactory = new RequestFactory();
	}
	/**To connect host.*/
	public void connectToHost(){
		serverController.connect();
	}
	/**
	 * To login.
	 * @param username is user name.
	 * @param password is password.
	 */
	public void login(String username, String password){
		Request request = requestFactory.requestLogin(username, password);
		sentRequest(request);
	}
	
	/**To get the user.*/
	public void getUser(){
		sentRequest(InfoRequestType.USER);
	}
	/**To get the chat.*/
	public void getChat(){
		sentRequest(InfoRequestType.CHATS);
	}
	/**To get the message.*/
	public void getMessage(int chatID){
		sentRequest(InfoRequestType.MESSAGES_IN_CHAT,String.valueOf(chatID));
	}
	
	/**
	 * To send the request.
	 * @param request is request.
	 */
	public void sentRequest(Request request){
		serverController.sentRequest(request);
	}
	
	/**
	 * To send the request.
	 * @param inforequestType is type of request.
	 * @param msg is message.
	 */
	public void sentRequest(InfoRequestType inforequestType,String msg){
		Request request = requestFactory.requestInfo(inforequestType,msg);
		serverController.sentRequest(request);
	}
	
	/**
	 * To send the request.
	 * @param inforequestType is type of request.
	 */
	public void sentRequest(InfoRequestType inforequestType){
		Request request = requestFactory.requestInfo(inforequestType);
		serverController.sentRequest(request);
	}
	
	/**To get friends.*/
	public void getFriends(){
		sentRequest(InfoRequestType.FRIENDS);
	}
	/**To get user.*/
	public void getUserInChat(int chatID){
		sentRequest(InfoRequestType.USERS_IN_CHAT,String.valueOf(chatID));
	}
	/**To get media.*/
	public void getMedia(int messageID){
		sentRequest(InfoRequestType.MEDIA_IN_MESSAGE,String.valueOf(messageID));
	}
	/**To get profile.*/
	public void getProfile(){
		sentRequest(InfoRequestType.PROFILE);
	}
	
	/**
	 * To get profile.
	 * @param userID is ID of user. 
	 */
	public  void  getProfile(int userID) {
		sentRequest(InfoRequestType.PROFILE,String.valueOf(userID));
		
	}
	/**
	 * To check if is login or not.
	 * @return true if log in.
	 */
	public boolean isLogined() {
		return logined;
	}
	
	/**
	 * Set the login.
	 * @param logined is boolean to set.
	 */
	public void setLogined(boolean logined) {
		this.logined = logined;
	}
	
	/**
	 * Set the user.
	 * @param user is user that want to set.
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * To display text when can't login.
	 * @param loginResponseStatus is status to show.
	 */
	public void canNotLogin(LoginResponseStatus loginResponseStatus){
		System.out.println("can not login :"+loginResponseStatus);
	}
	
	/**
	 * display text when password change.
	 */
	public void passwordChanged(){
		System.out.println("password change");
	}
}
