package co.bwsc.talker.client;

import co.bwsc.talker.client.ui.GUIEngine;

/**
 * This is Main.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class Main {
	public static void main(String[] args) {
		System.out.println(Main.class.getName() + " | Firing " + GUIEngine.class.getName() + "...");
		GUIEngine.invokeGUI(args);
	}
}
