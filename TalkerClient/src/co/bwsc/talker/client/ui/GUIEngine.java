package co.bwsc.talker.client.ui;

/**
 * This is HUIEngine.
 * @author Benjapol Worakan 5710546577 , Norawit Urailertprasert 5710546275 , Sanrasern Chaihetphon 5710547247
 */
public class GUIEngine {
	public static void invokeGUI(String[] args) {
		System.out.println(GUIEngine.class.getName() + " | Launching " + LoginPage.class.getName() + "...");
		LoginPage.invoke(args);
	}
}
